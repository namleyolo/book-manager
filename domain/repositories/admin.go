package repositories

import "context"

type IAdmin interface {
	Login(ctx context.Context, userId string ) (response interface{}, err error)
}
