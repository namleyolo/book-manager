package repositories

import (
	"context"
)

type IUser interface {
	GetUser(ctx context.Context, userId string ) (response interface{}, err error)
}
