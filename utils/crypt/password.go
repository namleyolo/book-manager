package crypt

import (
	"golang.org/x/crypto/bcrypt"
)

func CheckPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false

	}
	return true
}
