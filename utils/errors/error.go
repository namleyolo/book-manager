package errors

import (
	"encoding/json"

	err_micro "github.com/micro/go-micro/v2/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InternalMsg string
type ExternalMsg string
type Code int

const (
	ERROR_CODE_INVALID_DEVICE Code = iota
	ERROR_CODE_NOT_ACTIVE
	ERROR_CODE_USER_NOT_FOUND
	ERROR_CODE_USER_WRONG_PASSWORD
)

const (
	ERROR_MSG_INVALID_DEVICE      ExternalMsg = "Tài khoản của bạn đã được đăng nhập bằng thiết bị khác! "
	ERROR_MSG_NOT_ACTIVE          ExternalMsg = "Tài khoản của bạn đã bị khóa! "
	ERROR_MSG_USER_NOT_FOUND      ExternalMsg = "Tài khoản không tồn tại! "
	ERROR_MSG_USER_WRONG_PASSWORD ExternalMsg = "Mật tài khoản không đúng! "
)

type ErrorMsg struct {
	ExternalMsg ExternalMsg
	Code        Code
	InternalMsg InternalMsg
	error
}

func ErrorWithExternalMsg(err error, code Code, externalMsg ExternalMsg) error {

	return ErrorMsg{
		ExternalMsg: ExternalMsg(externalMsg),
		Code:        code,
		InternalMsg: InternalMsg(err.Error()),
		error:       err,
	}
}

func RecoveryError(err interface{}) error {
	if i, ok := err.(*status.Status); ok {
		return status.Errorf(i.Code(), i.Message())
	} else if i, ok := err.(*err_micro.Error); ok {
		return status.Errorf(codes.Code(i.Code), i.GetDetail())
	} else if i, ok := err.(ErrorMsg); ok {
		return status.Errorf(codes.Code(i.Code), string(i.ExternalMsg))
	}

	return status.Errorf(400, "%v", err)
}

func GetGrpcErrMessage(err error) string {
	if e, ok := status.FromError(err); ok {
		return e.Message()
	}

	if i, ok := err.(*err_micro.Error); ok {
		type CodeDetail struct {
			Code   int    `json:"code"`
			Detail string `json:"detail"`
		}
		var codeDetail CodeDetail
		err = json.Unmarshal([]byte(i.GetDetail()), &codeDetail)
		if err != nil {
			return i.Detail
		}
		return codeDetail.Detail
	}

	return err.Error()
}
