package configs

import (
	"github.com/spf13/viper"
)

//Config Project
type Config struct {
	Prefix      string `json:"prefix" mapstructure:"prefix"`
	Port        string `json:"port" mapstructure:"port"`
	ProxyPort   string `json:"proxy_port" mapstructure:"proxy_port"`
	ENV         string `json:"env" mapstructure:"env"`
	Job         bool   `json:"job" mapstructure:"job"`
	MaxPoolSize int    `json:"max_pool_size" mapstructure:"max_pool_size"`
	MongoURI    string `json:"mongo_uri" mapstructure:"mongo_uri"`
}

//LoadConfig Project runtime config.json
func LoadConfig() (*Config, error) {
	viper.AddConfigPath("./")
	viper.SetConfigType("json")
	viper.SetConfigName("config.json")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	result := &Config{}
	err = viper.Unmarshal(result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// LoadTestConfig load config for running tests
func LoadTestConfig(configPath string) *Config {
	viper.AddConfigPath(configPath)
	viper.SetConfigType("json")
	viper.SetConfigName("config_test.json")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	result := &Config{}
	err = viper.Unmarshal(result)
	if err != nil {
		panic(err)
	}
	return result
}
