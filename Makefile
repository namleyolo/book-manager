BINARY=engine
GOPATH:=$(shell go env GOPATH)
test:
	go test -mod vendor  -p 1 `go list ./... | grep -v /vendor/`
engine:
	go build -o ${BINARY}  ./cmd/main.go	
install:
	go get -u github.com/favadi/protoc-go-inject-tag 
	go get -u github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway 
	go get -u  github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 
	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc 
	go get -u google.golang.org/protobuf/cmd/protoc-gen-go
gen-proto-macos:
	 protoc \
	--openapiv2_out=./domains/dto/ \
	--openapiv2_opt logtostderr=true \
	--grpc-gateway_out=./domains/dto \
	--proto_path=./protobufs \
	--go-grpc_out=:./domains/dto \
	--go_out=./domains/dto ./protobufs/admin/*.proto
	protoc-go-inject-tag -input=./domains/dto/admin/admin.pb.go
	protoc-go-inject-tag -input=./domains/dto/user/user.pb.go