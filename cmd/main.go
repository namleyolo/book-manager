package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/core-api/domains/values_objects/context_application"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "gopkg.in/go-playground/validator.v9"

	"github.com/golang/glog"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	presenter_admin "luyentap1/presenter/admin"
	presenter_user "luyentap1/presenter/user"
	"luyentap1/utils/configs"
	"luyentap1/utils/errors"
	"luyentap1/utils/gpooling"
	"luyentap1/utils/logger"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	// command-line options:
	// gRPC server endpoint
	grpcServerEndpoint = flag.String("grpc-server-endpoint", "localhost:40001", "gRPC server endpoint")
)

func runProxy(ctx context.Context, cancel context.CancelFunc, config *configs.Config) error {
	lg, _ := logger.NewLogger("production")
	defer cancel()

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux(
		runtime.WithMetadata(func(c context.Context, req *http.Request) metadata.MD {
			for k, v := range req.Header {
				if len(v) > 0 {
					metadata.Pairs(k, v[0])
				}

			}
			return metadata.Pairs()
		}))
	opts := []grpc.DialOption{grpc.WithInsecure()}

	admin.register
	err := news.RegisterServiceNewsHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	_ = news.RegisterServiceUserHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)

	if err != nil {
		return err
	}
	lg.With(zap.Field{
		Key:    "port",
		Type:   zapcore.StringType,
		String: config.ProxyPort,
	}).Info("starting Proxy serverv1...")

	// Start HTTP server (and proxy calls to gRPC server endpoint)
	return http.ListenAndServe(":8081", mux)
}

func runGRPC(config *configs.Config) {

	pool_go_routine, _ := gpooling.NewPooling(config.MaxPoolSize)

	lg, _ := logger.NewLogger("production")
	srv := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_ctxtags.UnaryServerInterceptor(),
				grpc_opentracing.UnaryServerInterceptor(),
				grpc_recovery.UnaryServerInterceptor(grpc_recovery.WithRecoveryHandler(func(p interface{}) (err error) {
					lg.With(zap.Field{
						Key:       "context",
						Interface: p,
						Type:      zapcore.ReflectType,
					}).Error("error")

					return errors.RecoveryError(p)
				})),
				func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
					newCtx, _ := context_application.NewContext(ctx)

					ctx = newCtx
					logs := lg.With(
						zap.Field{
							Key:       "request",
							Interface: req,
							Type:      zapcore.ReflectType,
						},
					)

					md, ok := metadata.FromIncomingContext(ctx)

					if ok {
						logs = logs.With(
							zap.Field{
								Key:       "md-data",
								Interface: md,
								Type:      zapcore.ReflectType,
							},
						)
						if len(md["x-authenticated-userid"]) > 0 {
							newCtx.SetUserID(md["x-authenticated-userid"][0])
						}
					}

					resp, err = handler(ctx, req)

					if err != nil {
						err = errors.RecoveryError(err)
						logs.With(zap.Field{
							Key:       "error-context",
							Interface: err.Error(),
							Type:      zapcore.ReflectType,
						}, zap.Field{
							Key:       "TraceID",
							Interface: newCtx.TraceID,
							Type:      zapcore.ReflectType,
						}).Error("response-error")
					} else {
						logs.With(zap.Field{
							Key:       "response-context",
							Interface: resp,
							Type:      zapcore.ReflectType,
						}, zap.Field{
							Key:       "TraceID",
							Interface: newCtx.TraceID,
							Type:      zapcore.ReflectType,
						}).Info("response-success")
					}

					return
				},
			)),
	)

	mongo_conn := mongo.NewConnection(context.TODO(), mongo.OptMongoUri(config.MongoURI))

	newsDB := mongo.GetInstanceNews(mongo_conn, lg)

	userDB := mongo.GetInstanceUser(mongo_conn, lg)

	locationDB := mongo.GetInstanceLocation(mongo_conn, lg)

	newsService := news_service.Service{
		INews: newsDB,
	}


	locationService := location_service.Service{
		ILocation: locationDB,
	}
	// create presenter


	news.RegisterServiceUserServer(srv, presenter_user.NewPresenter().
		WithServiceUser(userService))

	news.RegisterServiceLocationServer(srv, presenter_location.NewsPresenter().
		WithService(locationService))

	sig := make(chan os.Signal, 1)

	signal.Notify(sig, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGKILL)

	pool_go_routine.Submit(func() {
		func() {
			select {
			case <-sig:
				lg.Warn("shutting down gRPC server...")
				srv.GracefulStop()
				pool_go_routine.Release()
			}
		}()
	})

	lis, err := net.Listen("tcp", ":"+config.Port)
	// Run service
	lg.With(zap.Field{
		Key:    "port",
		Type:   zapcore.StringType,
		String: config.Port,
	}).Info("starting gRPC serverv1...")

	if err != nil {
		panic(err)
	}
	if err := srv.Serve(lis); err != nil {
		panic(err)
	}
}

func main() {
	flag.Parse()
	defer glog.Flush()

	ctx, cancel := context_application.NewContext(context.Background())
	config, err := configs.LoadConfig()
	if err != nil {
		panic(err)
	}
	go func() {
		fmt.Println("Running GRPC")
		defer func() {
			r := recover()
			if r != nil {
				fmt.Println(r)
			}
		}()

		runGRPC(config)
	}()

	go func() {
		fmt.Println("Running ProxyGateway")

		defer func() {
			r := recover()
			if r != nil {
				fmt.Println(r)
			}
		}()

		if err := runProxy(ctx, cancel, config); err != nil {
			glog.Fatal(err)
		}
	}()

	select {}
}
