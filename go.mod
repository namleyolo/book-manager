module luyentap1

go 1.14

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.3.0 // indirect
	google.golang.org/genproto v0.0.0-20210323160006-e668133fea6a // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
